# keyforce

Enforce SSH keys on your K8s infrastructure

## Usage

      kubectl -n kube-system apply -f https://gitlab.com/tomkukral/keyforce/raw/master/deploy.yml
      kubectl -n kube-system apply -f deploy.yml

To immediately roll changes on ConfigMap to environment:

>     CONFIGHASH=$(kubectl get configmaps keyforce -n kube-system -o json | jq -r '.data[]' | md5)
>     kubectl patch ds/keyforce -n kube-system -p "{\"spec\":{\"template\":{\"metadata\":{\"labels\":{\"configHash\":\"$CONFIGHASH\"}}}}}"
